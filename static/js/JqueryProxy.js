
class AjaxProxy {

    constructor(data_url) {
        this.data_url = data_url
    }

    get_kyeword_counter(){
        $.ajax({
            url: "/api/parser/count_keyword_on_site/",
            type: "post",
            data: { url: this.data_url }
        }).done(function(result) {
            $('#button').hide();
            $('#result').remove();
            $('.modal-body').after('<div id="result">'+result+'</div>');
            $('#button').show();
        });

    }
}