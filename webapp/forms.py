from django import forms


class UrlForm(forms.Form):
    BEAUTIFUFULSOUP = 1
    REGEX = 2
    CHOICES_METHOD = (
        (BEAUTIFUFULSOUP, 'BeautifulSoup'),
        (REGEX, 'Regex'),
    )

    ajax_url = forms.URLField(required=False)
    method = forms.ChoiceField(choices=CHOICES_METHOD)
    url = forms.URLField(required=True)
