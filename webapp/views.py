import requests

from django.shortcuts import render
from django.views.generic import ListView

from webapp.forms import UrlForm
from lib.parse_request import ParseResponse


class HeaderKey(ListView):
    template_name = 'webapp/header_key/urls.html'
    form_class = UrlForm

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            cleaned_fields = form.cleaned_data
            response = requests.get(cleaned_fields['url'])
            parser = ParseResponse(response.content)
            if cleaned_fields['method'] == UrlForm.BEAUTIFUFULSOUP:
                context = parser.count_keyword_on_site()
            else:
                context = parser.count_keyword_on_site_regex()
        return render(
            request,
            self.template_name,
            {'context': context, 'form': form}
        )
