from rest_framework import serializers


class DataSerializer(serializers.Serializer):
    url = serializers.URLField()