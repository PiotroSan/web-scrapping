import json
import requests

from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from api.serializers import DataSerializer
from lib.parse_request import ParseResponse


class ParserViewSet(viewsets.ModelViewSet):
    serializer_class = DataSerializer
    permission_classes = [AllowAny]

    @action(methods=['post'], detail=False)
    def count_keyword_on_site(self, request):
        print('data', request.data)
        serializer = DataSerializer(data=request.data)
        if serializer.is_valid():
            response = requests.get(serializer.data['url'])
            parser = ParseResponse(response.content)
            context = parser.count_keyword_on_site()

        return Response(json.dumps(context))

    def get_queryset(self):
        pass