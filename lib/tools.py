
def get_index_element(sequence, index):
    try:
        return sequence[index]
    except IndexError:
        return []


def remove_witespace_in_sequence(sequence):
    return [text.strip() for text in sequence]