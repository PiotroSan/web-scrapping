import re

from bs4 import BeautifulSoup

from lib.tools import get_index_element, remove_witespace_in_sequence


class ParseResponse(object):

    def __init__(self, response):
        self.soup = BeautifulSoup(response, 'html.parser')
        self.keywords = self.find_meta_keywords(self.soup)

    def __getattr__(self, item):
        return getattr(self.soup, item)

    @staticmethod
    def find_meta_keywords(soup, name='meta', **attr):
        if not attr:
            attr = {'name': 'Keywords'}
        keyword_tag = soup.find(name, attr)
        if keyword_tag:
            return remove_witespace_in_sequence(
                keyword_tag['content'].split(',')
            )

    def count_keyword_on_site(self, keywords=None):
        if any([keywords, self.keywords]):
            return {
                keyword: len(self.soup.find_all(string=re.compile(keyword)))
                for keyword in keywords or self.keywords
            }

    def count_keyword_on_site_regex(self, keywords=None):
        if any([keywords, self.keywords]):
            text = self.soup.prettify()
            return {
                keyword: len(re.findall(re.compile(keyword), text))
                for keyword in keywords or self.keywords
            }

