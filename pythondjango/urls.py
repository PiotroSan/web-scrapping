"""pythondjango URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from rest_framework.routers import DefaultRouter

from webapp import views
from api import viewsets

router = DefaultRouter()
router.register(r'api/parser', viewsets.ParserViewSet, base_name='count_keyword_on_site')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('webapp/', views.HeaderKey.as_view(), name='webapp'),
    # path(r'^api/', include('rest_framework.urls'))
]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += router.urls